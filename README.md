## Parliamentary debates in Poland -- data processing

*Łukasz Nawaro, Jan Fałkowski, Jacek Lewkowicz*

*University of Warsaw, Faculty of Economic Sciences*

We process "Korpus Dyskursu Parlamentarnego" (Polish Parliamentary Corpus), expanding it with data scraped from the Sejm website to address the gaps regarding metadata on interpellations and queries.

Particular focus is placed on years 1991-2019 -- the democratically elected parliaments with full data. However, the code works for earlier years as well.

The main file is `provisions.py`, which should be modified accordingly to fit the goals. The regular expressions are defined in `regexs.py`, while directories -- in `config.yml`. Three possibilities of further analysis of terms defined in `main_regex` are shown:
* the `nierownosci` part shows a detailed analysis of one topic. All statements which contain the word are saved to .csv files in full. The resulting files may be large. Moreover, a co-occurrence analysis with words defined in the parameter `regexs_group` is performed.
* in the `main` part, the code goes through a lot of regular expressions, but does not save texts. Cheap memory-wise, but takes more time.
* `day` is detailed analysis by a single day thanks to `use_day=True` parameter. It may be useful when the topic is urgent (e.g. flood).

For inequalities, the regular expression [nN]ierównoś is case-insensitive with regards to the first letter and takes into account all the derivative forms of the "nierówność" word (nierówności – plural nominative, nierównościowa – feminine singular adjective etc.).

There are three ways to save statements:
* `remember_regex_texts` saves only the nearest vicinity of the analysed term (possibly multiple times for one statement)
* `remember_texts` saves the whole statement in which the analysed term occurs
* `tokenize_texts` is just like `remember_texts`, but with tokenization (using WordPunctTokenizer)

The .csv files with aggregate counts are then saved, depending on the suffix:
* `regex` counts the number of occurrences
* `binary` counts the number of statements in which the term occurred
* `author` and `binaryauthor` -- as above, but disaggregated to single authors
* `len` is the length (in characters) of analysed documents
* `div` is the number of statements in analysed documents, `divauthor` is further disaggregated to single authors
* `document` is the number of analysed documents

The counters' filenames start with `vs_` and are placed in the default `out_dir` directory defined in the configuration file. If texts are supposed to be saved as well, please set the `remember_texts` parameter to `True`, then the files will be saved with the `valltext_` prefix, split by regex, term, house, and type of document. The levels not aggregated are term, year, upper/lower house, and type of document. `all_levels` means that further levels (detailed type of document and author, if applicable) are also not aggregated.

The possible types of documents are parliamentary sitting, committee, questions/interpellations.

Counters from latter three lines are the same regardless of regular expressions, so you may run them just once. The method to save them is `save_counters()`, as opposed to `save_counters_regex()` which is different for every regular expression.

In `mps.py` file we transform the `.owl` file to a nicer form for further processing. The file `merge_mps.py` is an example how to use it to create a file which aggregates the `all_levels` files into parties. If an MP switched parties during the term, they are in the same row and they are not counted to their main party.

party\_data.pickle is a dictionary in which the key contains name, surname, house, and term, and the value – all the parties/parliamentary clubs the person belonged to in the given term. For instance, Jaroslaw\_Kaczynski\_\_Sejm3 belonged to ROP-PC (Ruch Odbudowy Polski-Porozumienie Centrum) in the 3rd term of Sejm (1997-2001). Please note that the Senat's terms are counted from 1989, while Sejm's 1989-1991 term was the 10th term of the Polish People's Republic's (PRL) Sejm.

This is work in progress, we apologise for any mistakes which may occur. Some sample files are in the `out/` directory.

### References

Ogrodniczuk, Maciej, and Bartłomiej Nitoń. "New developments in the Polish Parliamentary Corpus." Proceedings of the Second ParlaCLARIN Workshop. 2020.

Ogrodniczuk, Maciej. "The Polish Sejm Corpus." Proceedings of the Eighth International Conference on Language Resources and Evaluation (LREC'12). 2012.

### Funding

This work was supported by the University of Warsaw in the "Inicjatywa Doskonałości -- Uczelnia Badawcza" (The Excellence Initiative -- Research University) programme, task I.3.9. Mobilność i nierówności przez pryzmat nowych cyfrowych źródeł danych (Human mobility and inequalities as seen through digital datasources).
