import unidecode
import owlready2 as ow2
from collections import defaultdict
import pickle
from settings_create import load_settings
cfg = load_settings()

owl_filename = cfg['owl_filename']
party_data_filename = cfg['party_data_filename']

onto = ow2.get_ontology(owl_filename).load()

party_data = defaultdict(lambda: list())
for party in onto.get_instances_of(onto.PartySuborganization):
    term = party.get_name().split('_')[-1]
    party_name = party.nickname[0]
    if party_name == 'null':
        party_name = party.fullName[0]
        
    if party_name == 'niez.':
        continue
    
    members = []
    try:
        for member in party.hasMember:
            members.append(member)
    except:
        pass
    
    try:
        for member in party.hadMember:
            members.append(member)
    except:
        pass
    
    for member in members:
        party_data[unidecode.unidecode(member.get_name())].append(party_name)
        for member_inverse_properties in member.get_inverse_properties():
            if member_inverse_properties[0].firstName:
                member_data = member_inverse_properties[0]
                break
        party_data[unidecode.unidecode(
            '{}_{}__{}'.format(member_data.firstName[0], member_data.lastName[0].replace('-', '_'), term))].append(party_name)

party_data = {k: set(v) for k, v in party_data.items()}
for i in range(1,10):
    print(i)
    for izba in ['Sejm', 'Senat']:
        print(len({k: v for k, v in party_data.items() if '__{}{}'.format(izba, i) in k}))
    print('---')
pickle.dump(party_data, open(party_data_filename, 'wb'))