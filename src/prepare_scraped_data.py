import os
import pandas as pd
import pickle

from settings_create import load_settings
cfg = load_settings()

out_dir = cfg['scraped_dir']

scraped_data = {}
for filename in os.listdir(out_dir):
    if not filename.startswith('data_'):
        continue
    single_data = pd.read_pickle(os.path.join(out_dir, filename))
    file_data = filename.split('.')[0].split('_')
    file_key = '{}_{}'.format(file_data[2], file_data[1])
    if file_key not in scraped_data:
        scraped_data[file_key] = {}
    for i in range(len(single_data)):
        v = single_data[i]
        main_line = v['tytul'].split('\n')[0].split()
        v['iz'], v['nr'] = main_line[0], main_line[2]
        if 'Data wpływu_odp' in v.keys() or (v['nr'] not in scraped_data[file_key].keys()):
            scraped_data[file_key][v['nr']] = v

scraped_data['7_interpelacje']['25008'] = {'meta_charset': 'utf-8',
 'tytul': 'Interpelacja nr 25008\nw sprawie podejmowania przez Komisję Nadzoru Finansowego działań związanych z badaniem rękojmi prowadzenia działalności banku spółdzielczego z zachowaniem bezpieczeństwa wkładów i lokat w nim zgromadzonych',
 'w_sprawie': 'w sprawie podejmowania przez Komisję Nadzoru Finansowego działań związanych z badaniem rękojmi prowadzenia działalności banku spółdzielczego z zachowaniem bezpieczeństwa wkładów i lokat w nim zgromadzonych',
 'sekcje': 'Odpowiedź:',
 'Zgłaszający': 'Dariusz Seliga',
 'Adresat': 'prezes Rady Ministrów',
 'Data wpływu': '03-03-2014',
 'Data ogłoszenia': '13-03-2014 - posiedzenie nr 63',
 'Odpowiadający': 'Andrzej Jakubiak - przewodniczący Komisji Nadzoru Finansowego',
 'Data wpływu_odp': '28-03-2014',
 'Data ogłoszenia_odp': '03-04-2014 - posiedzenie nr 65',
 'iz': 'Interpelacja',
 'nr': '25008'}

scraped_data['7_interpelacje']['3792'] = {'meta_charset': 'utf-8',
 'tytul': 'Interpelacja nr 3792\nw sprawie sytuacji budownictwa mieszkaniowego w Polsce',
 'w_sprawie': 'w sprawie sytuacji budownictwa mieszkaniowego w Polsce',
 'sekcje': 'Odpowiedź:',
 'Zgłaszający': 'Jan Warzecha',
 'Adresat': 'minister transportu, budownictwa i gospodarki morskiej',
 'Data wpływu': '10-04-2012',
 'Data ogłoszenia': '26-04-2012 - posiedzenie nr 13',
 'Odpowiadający': 'Piotr Styczeń - podsekretarz stanu w Ministerstwie Transportu, Budownictwa i Gospodarki Morskiej',
 'Data wpływu_odp': '26-04-2012',
 'Data ogłoszenia_odp': '10-05-2012 - posiedzenie nr 14',
 'iz': 'Interpelacja',
 'nr': '3792'}

scraped_data['7_interpelacje']['3794'] = {'meta_charset': 'utf-8',
 'tytul': 'Interpelacja nr 3794\nw sprawie weryfikacji wysokości świadczeń rodzinnych oraz progów dochodowych, od których uzależnione jest prawo do świadczeń rodzinnych',
 'w_sprawie': 'w sprawie weryfikacji wysokości świadczeń rodzinnych oraz progów dochodowych, od których uzależnione jest prawo do świadczeń rodzinnych',
 'sekcje': 'Odpowiedź:',
 'Zgłaszający': 'Jan Warzecha',
 'Adresat': 'minister transportu, budownictwa i gospodarki morskiej',
 'Data wpływu': '10-04-2012',
 'Data ogłoszenia': '26-04-2012 - posiedzenie nr 13',
 'Odpowiadający': 'Marek Bucior - podsekretarz stanu w Ministerstwie Pracy i Polityki Społecznej',
 'Data wpływu_odp': '27-04-2012',
 'Data ogłoszenia_odp': '10-05-2012 - posiedzenie nr 14',
 'iz': 'Interpelacja',
 'nr': '3794'}

scraped_data['7_interpelacje']['50'] = {'meta_charset': 'utf-8',
 'tytul': 'Interpelacja nr 50\nw sprawie wątpliwości interpretacyjnych dotyczących planowania przez jednostki budżetowe i samorządowe zakłady budżetowe wydatków realizowanych w ramach zamówień publicznych',
 'w_sprawie': 'w sprawie wątpliwości interpretacyjnych dotyczących planowania przez jednostki budżetowe i samorządowe zakłady budżetowe wydatków realizowanych w ramach zamówień publicznych',
 'sekcje': 'Odpowiedź:',
 'Zgłaszający': 'Sławomir Zawiślak',
 'Adresat': 'minister finansów',
 'Data wpływu': '18-11-2011',
 'Data ogłoszenia': '01-12-2011 - posiedzenie nr 2',
 'Odpowiadający': 'Hanna Majszczyk - podsekretarz stanu w Ministerstwie Finansów',
 'Data wpływu_odp': '15-12-2011',
 'Data ogłoszenia_odp': '22-12-2011 - posiedzenie nr 4',
 'iz': 'Interpelacja',
 'nr': '50'}

scraped_data['4_interpelacje']['4856']['sekcje'] = 'Odpowiedź'

with open(cfg['scraped_data'], 'wb') as f:
    pickle.dump(scraped_data, f)