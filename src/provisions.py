from lxml import etree
from io import StringIO
import os
import re
import gc
from collections import defaultdict
import pandas as pd
from nltk.tokenize import WordPunctTokenizer
from datetime import datetime
from collections import Counter

from regexs import all_regexs, main_regexs, day_regexs, text_regexs, total_regexs
from settings_create import load_settings
cfg = load_settings()

out_dir = cfg['out_dir']
main_dir = cfg['main_dir']

scraped_data = pd.read_pickle(cfg['scraped_data'])
namespaces = {'t': 'http://www.tei-c.org/ns/1.0'}
days_to_skip = ['200105-sjm-zpxxx-05555-01', '200105-sjm-zpxxx-05557-01']  # błędne dni

class Provisions:
    def __init__(self, name, main_regex='', all_xmls=False, ann=False,
        remember_texts=False, remember_regex_texts=False,
        tokenize_texts=False, regexs_group={'': list()}, use_day=False,
        typs_to_skip=tuple()):
        self.main_regex = main_regex
        self.name = name
        self.all_xmls = all_xmls
        self.ann = ann
        self.remember_texts = remember_texts
        self.remember_regex_texts = remember_regex_texts
        self.tokenize_texts = tokenize_texts
        self.regexs_group = regexs_group
        self.use_day = use_day
        self.typs_to_skip = typs_to_skip
        with open('../in/polishstopwords.txt', 'r') as f:
            self.stoplist = f.read()

        self.surroundings = 150

    @staticmethod
    def find_scraped_date(header):
        note_type = header.find('.//t:note[@type="type"]', namespaces=namespaces).text
        term = header.find('.//t:note[@type="termNo"]', namespaces=namespaces).text
        number = header.find('.//t:note[@type="number"]', namespaces=namespaces).text
        part = header.find('.//t:note[@type="part"]', namespaces=namespaces).text
        part_value = int(part)
        title = header.find('.//t:titleStmt/t:title', namespaces=namespaces).text
        
        note_plural = {'zapytanie': 'zapytania', 'interpelacja': 'interpelacje',
                      'odpowiedź na zapytanie': 'zapytania',
                      'odpowiedź na interpelację': 'interpelacje'}
        
        if int(term) >= 7:
            return scraped_data['{}_{}'.format(term, note_plural[note_type.lower()])][number]['Data wpływu' + (
            '_odp' if part_value >= 2 else '') + ('_uzup'*(part_value - 2) if part_value > 2 else '')]
        
        single_data = scraped_data['{}_{}'.format(term, note_plural[note_type.lower()])][number]
        sections = single_data['sekcje'].split(', ')
        receivers = single_data['Adresat'].split('\n')
        
        dates = sorted([(k, v) for k, v in single_data.items() if k.startswith('Data wpływu')], key=lambda x: len(x))
        dates = [x[1] for x in dates]
        
        if note_type == 'Interpelacja' or note_type == 'Zapytanie':
            if title.endswith('- ponowna') or title.endswith('- ponowne'):
                for i_section, section in enumerate(sections):
                    if 'ponown' in section:
                        return dates[i_section+1]
                return dates[0]
            else:
                return dates[0]
        else:
            dates_resp = dates[1:]
            for i_section, section in enumerate(sections):
                if 'ponown' in section:
                    dates_resp.remove(dates[i_section+1])
            if len(dates_resp) == 1:
                return dates_resp[0]
            elif len(dates_resp) == 0:
                print(note_type, term, number, part, title, 'no dates_resp')
                return '00-00-0000'
            try:
                return dates_resp[part_value-1]
            except IndexError:
                print(note_type, term, number, part, title, 'KeyError')
                return '00-00-0000'
        # odpowiedzi to odpowiedzi, nie ma przedłużeń ani nic.

    @staticmethod
    def remove_xml_encoding(text):
        return text.replace(
            '<?xml version="1.0" encoding="UTF-8"?>', '<?xml version="1.0"?>').replace(
            "<?xml version='1.0' encoding='UTF-8'?>", "<?xml version='1.0'?>")

    def clean_header(self, text):
        return self.remove_xml_encoding(
            text.replace('<person xml:id="" role="speaker">', '<person xml:id="empty" role="speaker">'))

    def clean_structure(self, text):
        return self.remove_xml_encoding(text.replace('who="#"', 'who="#empty"'))

    @staticmethod
    def date_format(date):
        day, month, year = date.split('-')
        return '-'.join([year, month, day])
        
    def find_info(self, header):
        header_info = {}
        for info in header.findall('./t:fileDesc/t:sourceDesc/t:bibl/*', namespaces=namespaces):
            name = info.tag.split('}')[-1]
            if info_typ := info.attrib.get('type'):
                name = '{}_{}'.format(name, info_typ)
            header_info[name] = info.text
        header_info['title'] = header.find(
                        './/t:fileDesc/t:titleStmt/t:title', namespaces=namespaces).text
        try:
            header_info['date_full'] = header.find(
                            './/t:fileDesc/t:sourceDesc/t:bibl/t:date', namespaces=namespaces).text
        except AttributeError:  # no text
            scraped_date = self.date_format(self.find_scraped_date(header))
            header_info['date_full'] = scraped_date

        header_info['rok'] = header_info['date_full'][:4]
        return header_info

    def transform_paragraphs(self, div):
        return '\n'.join([x.text for x in div.findall('./t:u', namespaces=namespaces) if x.text] + [
                                    x.text for x in div.findall('./t:p', namespaces=namespaces) if x.text])

    @staticmethod
    def nested_to_dict(d):
        return {k: {k1: v1 for k1, v1 in v.items()} for k, v in d.items()}


    def create_ldas(self):
        from gensim import corpora
        from gensim.models.ldamodel import LdaModel
        import pickle
        dictionary = corpora.Dictionary(Files(only_most_recent=True, typs_to_skip=('komisje', 'interpelacje', ), yield_tokens=True))
        pickle.dump(dictionary, open('../out/ldatest_dict.pickle', 'wb'))
        # remove stop words and words that appear only once
        stop_ids = [
            dictionary.token2id[stopword]
            for stopword in self.stoplist
            if stopword in dictionary.token2id
        ]
        once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.items() if docfreq == 1]
        dictionary.filter_tokens(stop_ids + once_ids)  # remove stop words and words that appear only once
        dictionary.compactify()  # remove gaps in id sequence after words that were removed
        pickle.dump(dictionary, open('../out/ldafull_dict.pickle', 'wb'))
        print(dictionary)
        print('create lda model')

        for num_topics in [10, 20, 30, 50]:
            for alpha in ['auto', 'symmetric', 'asymmetric',
            5, 1, 0.5, 10**(-1), 5*(10**(-2)), 10**(-2), 5*(10**(-3)), 10**(-3),]:
                for eta in ['auto', 'symmetric',
                5, 1, 0.5, 10**(-1), 5*(10**(-2)), 10**(-2), 5*(10**(-3)), 10**(-3),]:
                    pars = 'x'.join([str(x) for x in [num_topics, alpha, eta]])
                    print(pars)
                    lda = LdaModel(corpus=Files(only_most_recent=False, typs_to_skip=('komisje', 'interpelacje', ), yield_tokens=True, dictionary=dictionary), 
                                   id2word=dictionary,
                                   num_topics=num_topics,
                                   update_every=1,
                                   chunksize=10000,
                                   alpha=alpha,
                                   eta=eta,
                                   passes=5)

                    pickle.dump(lda, open('../out/ldash_{}.pickle'.format(pars), 'wb'))


    def get_identifier(self, file_info, header_info, skip_year=False):
        identifier = (file_info['kadencja'], header_info.get('date_full', 'k{}'.format(file_info['kadencja'][-2:]))[:4], file_info['izba'], file_info['typ'], file_info['subtyp'] )
        if skip_year:
            identifier = (file_info['kadencja'], file_info['izba'], file_info['typ'], file_info['subtyp'] )
        if self.use_day:
            identifier = (file_info['kadencja'], header_info.get('date_full', 'k{}'.format(file_info['kadencja'][-2:]))[:4], file_info['izba'], file_info['typ'], file_info['subtyp'], file_info['dzien'] )
        return identifier


    def count_unique_words(self):
        wp = WordPunctTokenizer()
        counter = defaultdict(lambda: set())
        counter_word = defaultdict(lambda: 0)

        for file in Files(only_most_recent=False, yield_tokens=False):
            header_info = file['header_info']
            file_info = file['file_info']
            div_text = file['text']
            
            identifier = self.get_identifier(file_info, header_info, skip_year=True)

            for word in wp.tokenize(div_text.lower()):
                counter[identifier].add(word)
                counter_word[(*identifier, word)] += 1
        
        for k, v in counter.items():
            counter[k] = len(v)

        self.save_counter(dict(counter), 'wordcount')
        self.save_counter(dict(counter_word), 'wordsplitcount')




    def find_texts(self, agg_level=4, limit_authors=tuple()):
        counter = defaultdict(lambda: defaultdict(lambda: 0))
        counter_binary = defaultdict(lambda: defaultdict(lambda: 0))
        counter_authors = defaultdict(lambda: defaultdict(lambda: 0))
        counter_binary_authors = defaultdict(lambda: defaultdict(lambda: 0))

        counter_divs = defaultdict(lambda: 0)
        counter_divs_authors = defaultdict(lambda: 0)
        counter_documents = defaultdict(lambda: 0)
        counter_lens = defaultdict(lambda: 0)

        previous = []
        texts_part = []
        texts_regex_part = []
        tokens_part = []
        part_identifier = 'empty'

        if self.tokenize_texts:
            wp = WordPunctTokenizer()

        for file in Files(only_most_recent=False, yield_tokens=False, typs_to_skip=self.typs_to_skip):
            if '_'.join(file['file_levels'][:agg_level]) != part_identifier:
                if self.remember_texts:
                    self.save_texts_list(texts_part, part_identifier)
                    texts_part = []
                if self.remember_regex_texts:
                    self.save_texts_list(texts_regex_part, part_identifier, dataname='regexs')
                    texts_regex_part = []
                if self.tokenize_texts:
                    self.save_texts_list(tokens_part, part_identifier, dataname='tokens')
                    tokens_part = []

            header_info = file['header_info']
            file_info = file['file_info']
            div_text = file['text']
            div = file['div']

            identifier = self.get_identifier(file_info, header_info)

            counter_lens[identifier] += len(div_text)

            author_div = header_info.get('author', '')
            authors_div = []
            if not author_div:
                try:
                    authors_div = [x.attrib.get('who') for x in div.findall('./t:u', namespaces=namespaces)]
                    if authors_div:
                        author_div = Counter(authors_div).most_common(1)[0][0]
                    counter_divs_authors[(*identifier, author_div)] += 1
                except AttributeError:  # no utterance
                    pass
            else:
                counter_divs_authors[(*identifier, author_div)] += 1

            if limit_authors:
                to_limit = True
                for limit_author in limit_authors:
                    if re.search(limit_author, author_div):
                        print(limit_author, author_div, re.search(limit_author, author_div))
                        to_limit = False
                        break
                if to_limit:
                    continue

            if self.main_regex == '':
                continue

            for name_regex, single_regex in self.main_regex.items():
                if single_regex is not None:
                    regex_iter = re.finditer(single_regex, div_text)
                else:
                    regex_iter = [0]
                
                regex_exists = False
                for i in regex_iter:                    
                    if self.remember_regex_texts and not isinstance(i, int):
                        start = i.span()[0]-self.surroundings
                        end = i.span()[1]+self.surroundings
                        texts_regex_part.append({
                            'text': div_text[(start if start > 0 else 0) : (end if end < len(div_text) else len(div_text))],
                            'text_len': len(div_text),
                            'identifier': identifier,
                            'author_div': author_div,
                          **header_info, **file_info})
                    counter[name_regex][identifier] += 1
                    counter_authors[name_regex][(*identifier, author_div)] += 1
                    regex_exists = True

                if regex_exists:
                    counter_binary[name_regex][identifier] += 1
                    counter_binary_authors[name_regex][(*identifier, author_div)] += 1

                    if self.remember_texts:
                        texts_part.append({
                            'name_regex': name_regex,
                            'text': div_text,
                             'identifier': identifier,
                             'author_div': author_div,
                             **header_info, **file_info}
                        )

                    if self.tokenize_texts:
                        tokens_part.append({
                            'name_regex': name_regex,
                            'tokens': wp.tokenize(div_text),
                             'identifier': identifier,
                             'author_div': author_div,
                             **header_info, **file_info}
                        )

            previous = file['file_levels']
            part_identifier = '_'.join(previous[:agg_level])

        if self.remember_regex_texts:
            self.save_texts_list(texts_regex_part, part_identifier, dataname='regexs')
        if self.remember_texts:
            self.save_texts_list(texts_part, part_identifier)
        if self.tokenize_texts:
            self.save_texts_list(tokens_part, part_identifier, dataname='tokens')
             

        self.counter = self.nested_to_dict(counter)
        self.counter_binary = self.nested_to_dict(counter_binary)
        self.counter_authors = self.nested_to_dict(counter_authors)
        self.counter_binary_authors = self.nested_to_dict(counter_binary_authors)

        self.counter_divs = dict(counter_divs)
        self.counter_divs_authors = dict(counter_divs_authors)
        self.counter_documents = dict(counter_documents)
        self.counter_lens = dict(counter_lens)


    def save_texts_list(self, texts_list, file_identifier, dataname='texts'):
        print(pd.DataFrame(texts_list).columns)
        if not texts_list:
            return
        df = pd.DataFrame(texts_list).sort_values('date_full', ascending=False)
        df.to_csv(os.path.join(out_dir, 'valltext_{}part_{}_{}.csv'.format(dataname, self.name, file_identifier)))

    def save_counter(self, counters, counter_type, multiple=False):
        if not counters:
            return

        if multiple is False:
            counters = {'total': counters}

        for counter_name, counter in counters.items():
            print(counter_name)
            docs = pd.DataFrame(pd.Series(counter)).reset_index()
            docs.to_csv(os.path.join(out_dir, 'vs_{}_{}_{}_all_levels.csv'.format(counter_name, self.name, counter_type)))

            docs.groupby(
                ['level_0', 'level_1', 'level_2', 'level_3'])[0].sum().reset_index().to_csv(os.path.join(
                out_dir, '../out/vs_{}_{}_{}.csv'.format(counter_name, self.name, counter_type)))

    def save_counters_regex(self):
        self.save_counter(self.counter, 'regex', multiple=True)
        self.save_counter(self.counter_binary, 'binary', multiple=True)
        self.save_counter(self.counter_authors, 'author', multiple=True)
        self.save_counter(self.counter_binary_authors, 'binaryauthor', multiple=True)

    def save_counters(self):
        self.save_counter(self.counter_lens, 'len')
        self.save_counter(self.counter_divs, 'div')
        self.save_counter(self.counter_divs_authors, 'divauthor')
        self.save_counter(self.counter_documents, 'document')


class Files(Provisions):
    def __init__(self, only_most_recent=False, typs_to_skip=tuple(), yield_tokens=False, dictionary=None):
        super().__init__(name='')
        self.only_most_recent = only_most_recent
        self.typs_to_skip = typs_to_skip
        self.yield_tokens = yield_tokens
        self.dictionary = dictionary

    def __iter__(self):
        # import spacy
        # nlp = spacy.load('pl_core_news_md')

        wp = WordPunctTokenizer()
        for kadencja in sorted(os.listdir(os.path.join(main_dir)))[::-1]:
            if not os.path.isdir((os.path.join(main_dir, kadencja))) or '2023' in kadencja:
                continue
            if self.only_most_recent and kadencja != '2015-2019':
                continue
            for izba in sorted(os.listdir(os.path.join(main_dir, kadencja))):
                for typ in sorted(os.listdir(os.path.join(main_dir, kadencja, izba))):
                    if typ in self.typs_to_skip:
                        continue
                    print('starting', kadencja, izba, typ, datetime.now())
                    part_identifier = '{}_{}_{}'.format(kadencja, izba, typ)
                    for subtyp in sorted(os.listdir(os.path.join(main_dir, kadencja, izba, typ))):
                        for day in sorted(os.listdir(os.path.join(main_dir, kadencja, izba, typ, subtyp))):
                            if day in days_to_skip:
                                continue

                            structure_filename = os.path.join(main_dir, kadencja, izba, typ, subtyp, day, 'text_structure.xml')
                            header_filename = os.path.join(main_dir, kadencja, izba, typ, subtyp, day, 'header.xml')
                                
                            with open(structure_filename, 'r') as f:
                                text = self.clean_structure(f.read())
                                                               
                            try:
                                structure = etree.parse(StringIO(text))
                            except Exception as ex:
                                print('structure failure', structure_filename)

                            with open(header_filename, 'r') as f:
                                header = etree.parse(StringIO(self.clean_header(f.read())))
                            header_info = self.find_info(header)

                            divs = structure.findall('.//t:div', namespaces=namespaces)
                            bodies = structure.findall('.//t:body', namespaces=namespaces)

                            # div_texts = []
                            # for div in divs + bodies:
                            #     div_texts.append(self.transform_paragraphs(div))
                            # div_text = '\n'.join(div_texts)

                            for div in divs + bodies:
                                div_text = self.transform_paragraphs(div)

                                if self.yield_tokens:
                                    if self.dictionary is None:
                                        yield wp.tokenize(div_text.lower())
                                    else:
                                        yield self.dictionary.doc2bow(wp.tokenize(div_text.lower()))
                                else:
                                    yield {'file_info': {'kadencja': kadencja,
                                        'typ': typ,
                                        'subtyp': subtyp,
                                        'izba': izba,
                                        'dzien': day,
                                        }, 'file_levels': [kadencja, typ, subtyp, izba, day], 'header_info': header_info, 'text': div_text, 'div': div}


# l = Provisions(name='lda')
# l.create_ldas()
# n = Provisions(
#     main_regex=text_regexs,
#     name='nierownosci',
#     remember_texts=True,
#     regexs_group=all_regexs,
# )
# n.find_texts()
# n.save_counters_regex()
# n.save_counters()

main = Provisions(
    main_regex=main_regexs,
    name='main',
    remember_texts=True,
    typs_to_skip=('komisje', 'interpelacje',),
)
# main.count_unique_words()
main.find_texts()
main.save_counters_regex()
main.save_counters()

# day = Provisions(main_regex=day_regexs, name='day', remember_texts=False, use_day=True)
# day.find_texts()
# day.save_counters_regex()
# n.save_counters()
