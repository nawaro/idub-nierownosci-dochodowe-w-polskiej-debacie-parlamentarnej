import pandas as pd
from collections import defaultdict
import unidecode
import re
import os
from settings_create import load_settings
cfg = load_settings()
party_data_filename = cfg['party_data_filename']

terms_values = {'2015-2019': 8,'2011-2015': 7, '2007-2011': 6, '2005-2007': 5, '2001-2005': 4,
       '1997-2001': 3, '1993-1997': 2, '1991-1993': 1, '1989-1991': 10}
term_map = defaultdict(lambda: 0)  # no term defined / other term
for k, v in terms_values.items():
    term_map[k] = v
    
terms_values_senat = {'2015-2019': 9,'2011-2015':8, '2007-2011': 7, '2005-2007': 6, '2001-2005': 5,
       '1997-2001': 4, '1993-1997': 3, '1991-1993': 2, '1989-1991': 1}
term_map_senat = defaultdict(lambda: 0)
for k, v in terms_values_senat.items():
    term_map_senat[k] = v
    
def term_no(row):
    if row['izba'] == 'sejm':
        return term_map[row['kadencja']]
    else:
        return term_map_senat[row['kadencja']]

def author_id(row):
    term_data =  '__{}{}'.format(row['note_house'], int(row['note_termNo']))
    to_start = ['Posel', 'Poslanka', 'Senator', 'Senatorka', 'Przewodniczacyposel', 'Marszal', 'Wicemarszal']
    if str(row['author_div']).startswith('#'):
        mp = False
        if any([row['author_div'].startswith('#' + single_start) for single_start in to_start]):
            mp = True
        prefix = ''
    
        if 'podsekretarzstanu' in row['author_div'].lower():
            prefix = 'Podsekretarz_stanu_'
        elif 'sekretarzstanu' in row['author_div'].lower():
            prefix = 'Sekretarz_stanu_'
            
        larges = "".join([(" "+i if i.isupper() else i) for i in row['author_div']]).strip().split()
        return prefix + '_'.join(larges[(-2 if not mp else 2):]).replace('Sprawozdawca_', '') + term_data
    else:
        if ', ' in str(row['author_div']) or ' i ' in str(row['author_div']):
            return 'X (multiple authors)'
        prefix = ''
        if '- podsekretarz stanu' in str(row['author_div']):
            prefix = 'Podsekretarz_stanu_'
        elif '- sekretarz stanu' in str(row['author_div']):
            prefix = 'Sekretarz_stanu_'
        return prefix + unidecode.unidecode('_'.join(str(row['author_div']).split(' - ')[0].split()).replace('-', '_')) + term_data

party_data = pd.read_pickle(cfg['party_data_filename'])

tocs = []
for filename in os.listdir('../out/'):
#    if (not filename.endswith('_main_binaryauthor_all_levels.csv')) or (filename != 'vs_total_main_binaryauthor_all_levels.csv'):
#        continue
    if (not filename.endswith('_main_binaryauthor_all_levels.csv')):
        continue

    print(filename)
    authors = pd.read_csv('../out/' + filename, index_col=0)
    authors.columns = ['kadencja', 'rok', 'izba', 'typ', 'subtyp', 'author_div', 'count']
    authors['note_house'] = authors['izba'].str.capitalize()
    authors['note_system'] = authors['kadencja'].astype(str).apply(lambda x: 'III RP' if x > '1990' else 'PRL/III RP')

    authors['note_termNo'] = authors.apply(term_no, axis=1)
    authors['author_id'] = authors.apply(author_id, axis=1)
    authors['author_party'] = authors['author_id'].map(party_data)
    authors['author_party_str'] = authors['author_party'].apply(lambda x: ', '.join(x) if hasattr(x, '__iter__') else '')
    toc = pd.DataFrame(authors.loc[authors['note_termNo'] > 0].groupby(['note_termNo', 'kadencja', 'note_system', 'note_house', 'author_party_str', 'typ'])['count'].sum()).reset_index()
    toc['term'] = filename.split('_')[1]
    tocs.append(toc)

party_terms = pd.concat(tocs).sort_values('term')
party_terms_total = party_terms.loc[party_terms['term'] == 'total']
party_terms_all = party_terms.merge(party_terms_total, on=party_terms.columns.tolist()[:6])
party_terms_all['share'] = 100 * party_terms_all['count_x'] / party_terms_all['count_y']

party_terms_all.to_csv('../out/party_terms.csv')
