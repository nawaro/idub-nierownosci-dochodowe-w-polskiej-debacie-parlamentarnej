import yaml

def load_settings():
	cfg = yaml.safe_load(open('config.yml', 'r'))
	return cfg