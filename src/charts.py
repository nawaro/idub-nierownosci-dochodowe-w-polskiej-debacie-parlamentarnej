from regexs import all_regexs, main_regexs, day_regexs, text_regexs

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('whitegrid')
sns.set(font_scale=1.5)

def read_csvs():
	bezrobocie = pd.read_csv('../in/bezrobocie.csv', index_col=0)
	for col in bezrobocie.columns:
	    bezrobocie[col] = bezrobocie[col].apply(lambda x: x.split('\n')[0]).str.replace(',', '.').str.replace('*', '')

	bezrobocie.index = [int(x.split('\n')[0]) for x in bezrobocie.index]
	bezrobocie = bezrobocie.loc[bezrobocie.index < 2022]
	bezrobocie = bezrobocie.loc[bezrobocie.index > 0].astype(float)
	bezrobocie['avg'] = bezrobocie.mean(axis=1)
	bezrobocie = bezrobocie.loc[list(bezrobocie.index)[::-1]]

	bonds = pd.read_csv('../in/data/bond_yields.csv')
	bonds['year'] = bonds['Date'].str[:4]
	bonds = pd.DataFrame(bonds.groupby('year')['10Y_Open'].mean())
	bonds.columns = ['avg']
	bonds.index = bonds.index.astype(int)

	currencies = pd.read_csv('../in/data/daily_currencies.csv')
	currencies['year'] = currencies['Date'].str[:4]

	all_currencies = {}
	for currency_name in ['USD', 'EUR', 'CHF']:
	    one_currency = pd.DataFrame(currencies.groupby('year')['PLN{}_Open'.format(currency_name)].mean())
	    one_currency.columns = ['avg']
	    one_currency.index = one_currency.index.astype(int)
	    all_currencies[currency_name] = one_currency

	zabojstwa = pd.read_csv('../in/data/zabojstwa.csv')
	zabojstwa['Rok'] = zabojstwa['Rok'].astype(int)
	zabojstwa.set_index('Rok', inplace=True)
	zabojstwa.columns = ['avg']
	zabojstwa['avg'] = zabojstwa['avg'].str.replace(',', '').astype(int)

	datas = {
	    'bezrobocie': bezrobocie,
	    'bonds': bonds,
	    'zabojstwa': zabojstwa,
	    **all_currencies,
	}

	return datas

rol_window = 4
method = 'binaryauthor'
en_names = {'bezrobocie': 'unemployment', 'zabojstwa': 'homicides'}
create_charts = False

all_dfs = []

for data_name, data_df in read_csvs().items():
    if data_name not in ['bezrobocie']:#, 'zabojstwa']:
        continue
    data_df.rename(columns={'avg': data_name}, inplace=True)
    for main_name, topics in {'main': main_regexs}.items():
        for topic in topics:
            print(topic)
            main = pd.read_csv('../out/vs_total_nierownosci_divauthor_all_levels.csv'.format(method), index_col=0)
            main['level_5'] = main['level_5'].fillna('')
            main = main.loc[(main['level_2'] == 'sejm') & (main['level_3'] == 'posiedzenia') &
                           (~(main['level_5'].str.startswith('#Marszalek')) & ~(
                           main['level_5'].str.startswith('#Wicemarszalek')) & ~(
                           main['level_5'] == '#TeresaDobielinskaEliszewska') & ~(
                           main['level_5'] == '#komentarz'))]
            main = main.groupby('level_1').sum().reset_index()

            df = pd.read_csv('../out/vs_{}_{}_{}_all_levels.csv'.format(topic, main_name, method), index_col=0)
            df['level_5'] = df['level_5'].fillna('')
            df = df.loc[(df['level_2'] == 'sejm') & (df['level_3'] == 'posiedzenia') &
                           (~(df['level_5'].str.startswith('#Marszalek')) & ~(
                           df['level_5'].str.startswith('#Wicemarszalek')) & ~(
                           df['level_5'] == '#TeresaDobielinskaEliszewska') & ~(
                           df['level_5'] == '#komentarz'))]
            df = df.groupby('level_1').sum().reset_index()

            df = df.merge(main, on=[x for x in df.columns if x.startswith('level_')], suffixes=('_regex', '_all'))
            df.rename(columns={'level_0': 'kadencja', 'level_1': 'rok', 'level_2': 'izba', 'level_3': 'typ'}, inplace=True)
            df['share'] = df['0_regex'] / df['0_all']

            df.set_index('rok', inplace=True)
            df = df.loc[df.index >= 1991]
            df['name'] = topic
            df['rolling_{}'.format(rol_window)] = df['share'].rolling(rol_window).mean()
            data_df['{}_rolling_{}'.format(data_name, rol_window)] = data_df[
                data_name].rolling(rol_window).mean()
            all_dfs.append(df.join(data_df[[data_name,
                                            '{}_rolling_{}'.format(data_name, rol_window)]]))

            if create_charts:
                fig, ax = plt.subplots(figsize=(12,8))
        #         ax.plot(df['0_regex'].rolling(rol_window).mean(), color='blue', label='count')
                ax1 = ax.twinx()
                ax.plot(df['share'].rolling(1).mean(), color='blue', label='share "{}" 1-year'.format(topic))
                ax.plot(df['share'].rolling(rol_window).mean(), color='red', label='share "{}" {}-year'.format(
                    topic, rol_window))
                ax.legend(loc='upper left')

                scale_factor = 1 #df['share'].rolling(rol_window).mean().max() / data_df['avg'].rolling(rol_window).mean().max()
                ax1.plot(data_df[data_name].rolling(rol_window).mean() * scale_factor, color='orange', label='{}'.format(en_names[data_name]))
                ax1.legend()
                plt.title(topic)

                plt.savefig('../charts/{}_{}_x{}_rol{}.png'.format(method, topic, data_name, rol_window))

                ax.set_ylim(0, df['share'].max()*1.1)
                plt.savefig('../charts/{}_{}_x{}_rol{}_ax0.png'.format(method, topic, data_name, rol_window))
                plt.show()
                plt.close()

pd.concat(all_dfs).to_csv('../out/all_dfs.csv')